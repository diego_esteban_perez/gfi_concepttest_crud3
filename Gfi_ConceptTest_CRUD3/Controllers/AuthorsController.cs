﻿using System.Threading.Tasks;

using Gfi_ConceptTest_CRUD3.Entities;
using Gfi_ConceptTest_CRUD3.Models;
using Gfi_ConceptTest_CRUD3.Repository;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RestSharp;

namespace Gfi_ConceptTest_CRUD3.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthorsController : ControllerBase
    {
        private readonly IAuthorRepository authorRepository;

        public AuthorsController(IAuthorRepository authorRepository) 
        {
            this.authorRepository = authorRepository;
        }
        // GET: api/Authors
        [HttpGet]
        public async Task<ActionResult<string>> Get()
        {
            IRestResponse response = await authorRepository.GetAuthors();
            if (response.ResponseStatus == ResponseStatus.Error)
            {
                return Status500Error("There was a problem getting authors.");
            }

            return response.Content;
        }

        // GET: api/Authors/5
        [HttpGet("{id}", Name = "GetAuthor")]
        public async Task<ActionResult<string>> Get(int id)
        {
            IRestResponse response = await authorRepository.GetAuthor(id);
            if (response.ResponseStatus == ResponseStatus.Error)
            {
                return Status500Error("There was a problem getting author.");
            }

            string author = response.Content;

            if (author == string.Empty)
            {
                return NotFound();
            }

            return author;
        }

        // POST: api/Authors
        [HttpPost]
        public async Task<ActionResult> Post([FromBody] AuthorDTO authorDTO)
        {
            IRestResponse response = await authorRepository.Insert(authorDTO);
            if (response.ResponseStatus == ResponseStatus.Error)
            {
                return Status500Error("There was a problem inserting author.");
            }
            
            Author author = JsonConvert.DeserializeObject<Author>(response.Content);
            return new CreatedAtRouteResult("GetAuthor", new { id = author.Id }, author);
        }

        // PUT: api/Authors/5
        [HttpPut("{id}")]
        public async Task<ActionResult> Put(int id, [FromBody] AuthorDTO author)
        {
            IRestResponse response = await authorRepository.Modify(id, author);
            if (response.ResponseStatus == ResponseStatus.Error)
            {
                return Status500Error("There was a problem modifying author.");
            }
            return Ok();
        }

        // DELETE: api/Authors/5
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            IRestResponse response = await authorRepository.Delete(id);
            if (response.ResponseStatus == ResponseStatus.Error)
            {
                return Status500Error("There was a problem modifying author.");
            }
            return Ok();
        }

        //

        private ActionResult Status500Error(string errorMsg) 
        {
            return StatusCode(StatusCodes.Status500InternalServerError, errorMsg);
        }
    }
}
