﻿using Gfi_ConceptTest_CRUD3.Models;
using RestSharp;
using System.Threading.Tasks;

namespace Gfi_ConceptTest_CRUD3.Repository
{
    public interface IAuthorRepository
    {
        Task<IRestResponse> Insert(AuthorDTO authorDTO);
        Task<IRestResponse> GetAuthor(int id);
        Task<IRestResponse> GetAuthors();
        Task<IRestResponse> Modify(int id, AuthorDTO author);
        Task<IRestResponse> Delete(int id);
    }
}
