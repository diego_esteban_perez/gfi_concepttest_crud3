﻿using Gfi_ConceptTest_CRUD3.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using RestSharp;
using System;
using System.IO;
using System.Threading.Tasks;

namespace Gfi_ConceptTest_CRUD3.Repository
{
    public class AuthorRepository : IAuthorRepository
    {
        private readonly ILogger logger;
        private readonly string apiBaseUrl;
        private RestClient client;

        public AuthorRepository(IConfiguration configuration, ILogger<AuthorRepository> logger)
        {
            apiBaseUrl = configuration.GetSection("apiBaseUrl").Value;
            client = new RestClient(apiBaseUrl);
            this.logger = logger;
        }

        public async Task<IRestResponse> GetAuthor(int id)
        {
            IRestResponse response = new RestResponse();
            try
            {
                var apiResource = "Authors/{id}";
                var request = new RestRequest(apiResource, Method.GET);
                request.AddUrlSegment("id", id);
                response = await client.ExecuteTaskAsync<string>(request);
            }
            catch (IOException ex)
            {
                logger.LogError(ex.Message);
                response.ResponseStatus = ResponseStatus.Error;
            }
            return response;
        }

        public async Task<IRestResponse> GetAuthors()
        {
            IRestResponse response = new RestResponse();
            try
            {
                var apiResource = "Authors";
                var request = new RestRequest(apiResource, Method.GET);
                response = await client.ExecuteTaskAsync<string>(request);
            }
            catch (IOException ex)
            {
                logger.LogError(ex.Message);
                response.ResponseStatus = ResponseStatus.Error;
            }
            return response;
        }

        public async Task<IRestResponse> Insert(AuthorDTO authorDTO)
        {
            IRestResponse response = new RestResponse();
            try
            {
                var apiResource = "Authors";
                var request = new RestRequest(apiResource, Method.POST);
                request.AddJsonBody(authorDTO);
                response = await client.ExecuteTaskAsync<string>(request);
            }
            catch (Exception ex)
            {
                logger.LogError(ex.Message);
                response.ResponseStatus = ResponseStatus.Error;
            }
            return response;
        }

        public async Task<IRestResponse> Modify(int id, AuthorDTO author)
        {
            IRestResponse response = new RestResponse();
            try
            {
                var apiResource = "Authors/{id}";
                var request = new RestRequest(apiResource, Method.PUT);
                request.AddUrlSegment("id", id);
                request.AddJsonBody(author);
                response = await client.ExecuteTaskAsync<string>(request);
            }
            catch (Exception ex)
            {
                logger.LogError(ex.Message);
                response.ResponseStatus = ResponseStatus.Error;
            }
            return response;
        }

        public async Task<IRestResponse> Delete(int id)
        {
            IRestResponse response = new RestResponse();
            try
            {
                var apiResource = "Authors/{id}";
                var client = new RestClient(apiBaseUrl);
                var request = new RestRequest(apiResource, Method.DELETE);
                request.AddUrlSegment("id", id);
                response = await client.ExecuteTaskAsync<string>(request);
            }
            catch (Exception ex)
            {
                logger.LogError(ex.Message);
                response.ResponseStatus = ResponseStatus.Error;
            }
            return response;
        }
    }
}
